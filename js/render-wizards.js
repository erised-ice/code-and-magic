'use strict';

(() => {
  const userDialog = document.querySelector('.setup');

  const chosenWizard = {
    names: [],
    secondNames: [],
    coatColors: [],
    eyesColors: []
  };

  const similarListElement = document.querySelector('.setup-similar-list');
  const similarWizardTemplate = document.querySelector('#similar-wizard-template')
    .content
    .querySelector('.setup-similar-item');

  const createRandomWizardProperty = (array, chosenArray) => {
    const result = window.utils.getRandomElement(array);

    window.utils.removeElementFromArray(array, result);

    const hasResult = chosenArray.includes(result);

    return hasResult ? createRandomWizardProperty(array, chosenArray) : result;
  }

  const createWizardData = () => {
    const randomName = createRandomWizardProperty(window.mocks.WIZARD.names, chosenWizard.names);
    const randomSecondName = createRandomWizardProperty(window.mocks.WIZARD.secondNames, chosenWizard.secondNames);
    const randomCoatColor = createRandomWizardProperty(window.mocks.WIZARD.coatColors, chosenWizard.coatColors);
    const randomEyesColor = createRandomWizardProperty(window.mocks.WIZARD.eyesColors, chosenWizard.eyesColors);

    return {
      name: randomName,
      secondName: randomSecondName,
      coatColor: randomCoatColor,
      eyesColor: randomEyesColor
    };
  }

  const constructWizard = () => {
    const wizard = createWizardData();

    chosenWizard.names.push(wizard.name);
    chosenWizard.secondNames.push(wizard.secondName);
    chosenWizard.coatColors.push(wizard.coatColor);
    chosenWizard.eyesColors.push(wizard.eyesColor);

    return wizard;
  }

  const wizardsArray = Array.from({ length: 4 }, constructWizard);

  const createWizardTemplate = (wizard) => {
    const wizardElement = similarWizardTemplate.cloneNode(true);

    wizardElement.querySelector('.setup-similar-label').textContent = `${wizard.name} ${wizard.secondName}`;
    wizardElement.querySelector('.wizard-coat').style.fill = wizard.coatColor;
    wizardElement.querySelector('.wizard-eyes').style.fill = wizard.eyesColor;

    return wizardElement;
  };

  const createWizardsCollection = (objectsArray) => {
    const fragment = document.createDocumentFragment();

    objectsArray.forEach((element) => {
      const wizard = createWizardTemplate(element);

      fragment.appendChild(wizard);
    });

    return fragment;
  }

  const wizardsCollection = createWizardsCollection(wizardsArray);

  similarListElement.appendChild(wizardsCollection);
  userDialog.querySelector('.setup-similar').classList.remove('hidden');
})();
