'use strict';

(() => {
  const setupWizard = document.querySelector('.setup-wizard');
  const wizardCoat = setupWizard.querySelector('.wizard-coat');
  const wizardEyes = setupWizard.querySelector('.wizard-eyes');
  const fireball = document.querySelector('.setup-fireball-wrap');

  const setColor = (target, colorsArray, styleProperty, name) => {
    const currentColor = window.getComputedStyle(target)[styleProperty];
    const randomColor = window.utils.getRandomElement(colorsArray);

    if (currentColor === randomColor) {
      setColor(target, colorsArray, styleProperty, name);
    } else {
      target.style[styleProperty] = randomColor;
      document.querySelector(`input[name=${name}]`).value = randomColor;
    }
  }

  wizardCoat.addEventListener('click', (evt) => {
    setColor(evt.target, window.mocks.SETUP_WIZARD.coatColors, 'fill', 'coat-color');
  });

  wizardEyes.addEventListener('click', (evt) => {
    setColor(evt.target, window.mocks.SETUP_WIZARD.eyesColors, 'fill', 'eyes-color');
  });

  fireball.addEventListener('click', (evt) => {
    setColor(evt.target, window.mocks.SETUP_WIZARD.fireballColors, 'backgroundColor', 'fireball-color');
  })
})();
