'use strict';

(() => {
  const userDialog = document.querySelector('.setup');
  const userDialogHandler = userDialog.querySelector('.upload');

  userDialogHandler.addEventListener('mousedown', (evt) => {
    evt.preventDefault();

    let startCoords = {
      x: evt.clientX,
      y: evt.clientY
    };

    let isDragged = false;

    const onMouseMove = (moveEvt) => {
      moveEvt.preventDefault();
      isDragged = true;

      const shift = {
        x: startCoords.x - moveEvt.clientX,
        y: startCoords.y - moveEvt.clientY
      };

      startCoords = {
        x: moveEvt.clientX,
        y: moveEvt.clientY
      };

      userDialog.style.top = `${userDialog.offsetTop - shift.y}px`;
      userDialog.style.left = `${userDialog.offsetLeft - shift.x}px`;
    };

    const onMouseUp = (upEvt) => {
      upEvt.preventDefault();

      document.removeEventListener('mousemove', onMouseMove);
      document.removeEventListener('mouseup', onMouseUp);

      if (isDragged) {
        const onClickPreventDefault = (evt) => {
          evt.preventDefault();

          userDialogHandler.removeEventListener('click', onClickPreventDefault);
        };

        userDialogHandler.addEventListener('click', onClickPreventDefault);
      }
    }

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);
  });
})();
