const CLOUD = {
  width: 420,
  height: 270,
  x: 100,
  y: 10,
  color: '#fff',
  shadowGap: 10,
  shadowColor: 'rgba(0, 0, 0, 0.7)'
};

const COLUMN = {
  width: 40,
  maxHeight: 150,
  gap: 50
};

const TEXT_COLOR = '#000';
const FIRST_COLUMN_X = CLOUD.x + 50;
const FIRST_PLAYER_COLOR = 'rgba(255, 0, 0, 1)';

const renderCloud = (ctx, x, y, color) => {
  ctx.fillStyle = color;
  ctx.fillRect(x, y, CLOUD.width, CLOUD.height);
};

const renderCloudText = (ctx, text, x, y) => {
  ctx.fillStyle = TEXT_COLOR;
  ctx.font = '16px PT Mono';
  ctx.fillText(text, x, y);
};

const getMaxTime = (timesArray) => {
  return timesArray.reduce((maxTime, item) => maxTime < item ? item : maxTime);
};

const renderColumns = (ctx, players, playerResults) => {
  const maxTime = getMaxTime(playerResults);

  players.forEach((player, index) => {
    const columnHeight = (COLUMN.maxHeight * playerResults[index]) / maxTime;
    const columnX = FIRST_COLUMN_X + (COLUMN.width + COLUMN.gap) * index;
    const columnY = CLOUD.y + CLOUD.height - 40 - columnHeight;

    const saturation = `${Math.random() * 100}%`;
    const columnColor = (player === 'Вы') ? FIRST_PLAYER_COLOR : `hsl(232, 100%, ${saturation}`;

    ctx.fillStyle = columnColor;
    ctx.fillRect(columnX, columnY, COLUMN.width, columnHeight);

    ctx.fillStyle = TEXT_COLOR;
    ctx.fillText(player, columnX, CLOUD.y + CLOUD.height - 15 );
    ctx.fillText(Math.floor(playerResults[index]), columnX, columnY - 10 );
  });
}

window.renderStatistics = (ctx, names, times) => {
  renderCloud(ctx, CLOUD.x + CLOUD.shadowGap, CLOUD.y + CLOUD.shadowGap, CLOUD.shadowColor);
  renderCloud(ctx, CLOUD.x, CLOUD.y, CLOUD.color);

  renderCloudText(ctx,'Ура вы победили!', CLOUD.x + 60, CLOUD.y + 20);
  renderCloudText(ctx,'Список результатов:', CLOUD.x + 60, CLOUD.y + 40);

  renderColumns(ctx, names, times);
}
