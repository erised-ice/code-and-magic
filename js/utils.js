'use strict';

(() => {
  const ENTER_KEY = 'Enter';

  window.utils = {
    isEnterEvent: (evt, action) => {
      if (evt.key === ENTER_KEY) {
        action();
      }
    },
    getRandomElement: (array) => {
      const randomIndex = Math.floor(Math.random() * array.length);

      return array[randomIndex];
    },
    removeElementFromArray: (array, element) => {
      const index = array.indexOf(element);

      if (index !== -1) {
        array.splice(index, 1);
      }
    }
  }
})();
