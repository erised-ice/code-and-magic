'use strict';

(() => {
  const userNameInput= document.querySelector('.setup-user-name');
  const minNameLength = 2;
  const maxNameLength = 25;

  userNameInput.addEventListener('invalid',  () => {
    switch (true) {
      case userNameInput.validity.tooShort:
        userNameInput.setCustomValidity(`Имя должно состоять минимум из ${minNameLength}-х символов`);
        break;
      case userNameInput.validity.tooLong:
        userNameInput.setCustomValidity(`Имя не должно превышать ${maxNameLength}-ти символов`);
        break;
      case userNameInput.validity.valueMissing:
        userNameInput.setCustomValidity('Обязательное поле');
        break;
      default:
        userNameInput.setCustomValidity('');
        break;
    }
  });
})();
