'use strict';

(() => {
  const userDialog = document.querySelector('.setup');
  const userDialogCoords = {
    top: userDialog.style.top,
    left: userDialog.style.left
  }
  const userDialogOpenElement = document.querySelector('.setup-open');
  const userDialogCloseElement = userDialog.querySelector('.setup-close');
  const userNameInput = userDialog.querySelector('.setup-user-name');

  let isOpenUserDialog = false;

  const openUserDialog = () => {
    if (!isOpenUserDialog) {
      userDialog.style.top = userDialogCoords.top;
      userDialog.style.left = userDialogCoords.left;
    }

    userDialog.classList.remove('hidden');
    isOpenUserDialog = true;
  };

  userDialogOpenElement.addEventListener('keydown', (evt) => {
    window.utils.isEnterEvent(evt, openUserDialog);
  });

  const closeUserDialog = () => {
    userDialog.classList.add('hidden');
    isOpenUserDialog = false;
  }

  userDialogOpenElement.addEventListener('click', openUserDialog);
  userDialogCloseElement.addEventListener('click', closeUserDialog);

  userDialogCloseElement.addEventListener('keydown', (evt) => {
      window.utils.isEnterEvent(evt, closeUserDialog);
    }
  );

  document.addEventListener('keydown', (evt) => {
    if (document.activeElement !== userNameInput && evt.key === 'Escape' && isOpenUserDialog) {
      closeUserDialog();
    }
  })
})();
